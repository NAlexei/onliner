import unittest

from steps.onliner_steps.onliner_steps import OnlinerSteps
from page.onliner_page import OnlinerPage

EXPECTED_CART_TEXT = "Корзина"


class OnlinerTest(unittest.TestCase):

    onliner_steps = OnlinerSteps()

    @classmethod
    def tearDownClass(cls):
        cls.onliner_steps.close_browser()

    def test_open_main_page(self):
        self.onliner_steps.open_main_page()

    def test_verify_cart_name(self):
        self.onliner_steps.open_main_page()
        self.onliner_steps.wait_for_element_is_displayed(OnlinerPage.cart, timeout_seconds=5)
        cart_text = self.onliner_steps.get_element_text(OnlinerPage.cart)
        self.assertEqual(cart_text, EXPECTED_CART_TEXT)
