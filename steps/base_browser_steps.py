from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from api.webdriver_provider import get_driver


class BaseBrowserSteps(object):

    def __init__(self):
        self.driver = get_driver()

    def open_page(self, url):
        self.driver.get(url)

    def wait_for_element_is_displayed(self, locator, timeout_seconds=10):
        WebDriverWait(self.driver,timeout_seconds).until(EC.visibility_of_element_located(locator))

    def get_element_text(self, locator):
        element = WebDriverWait(self.driver,10).until(EC.visibility_of_element_located(locator))
        return element.text

    def refresh_page(self):
        self.driver.refresh()

    def close_browser(self):
        self.driver.quit()

