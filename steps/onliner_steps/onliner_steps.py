from steps.base_browser_steps import BaseBrowserSteps

MAIN_PAGE_URL = 'https:\\onliner.by'


class OnlinerSteps(BaseBrowserSteps):

    def open_main_page(self):
        self.open_page(MAIN_PAGE_URL)
